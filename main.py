import itertools

from Synesthesia_GAN.EMNIST_data_set_loader.EMNIST_data_set_loader import EMNISTLoader, UsageType
from Synesthesia_GAN.CNN.CNN import CNN
from Synesthesia_GAN.gif_creator import gif_creator
from Synesthesia_GAN.GAN.GAN import GAN
from Synesthesia_GAN.batch_generator.batch_generator import BatchGenerator
import Synesthesia_GAN.Utilities as utils
import tensorflow as tf
from matplotlib.colors import hsv_to_rgb
import numpy as np

loader = EMNISTLoader()
# train_images, train_labels, test_images, test_labels, data_shape = \
#     loader.get_emnist_database(usage=UsageType.uppercase,
#                                color_map_conversion='hsv',
#                                convert_to_synesthetic_color=True)

# train_dataset, test_dataset, data_shape = \
#     loader.get_emnist_database(usage=UsageType.uppercase,
#                                color_map_conversion='hsv',
#                                convert_to_synesthetic_color=True, merge_labels_with_datasets=True)

# train_images, train_labels, test_images, test_labels, data_shape = loader.load_dataset_from_pickle("D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Datasets\\EMNIST_16x16_uppercase_Impl_SynContColor_hsv_Blackbckgrnd_2020-05-28.pkl")
train_dataset, test_dataset, data_shape = loader.load_dataset_from_pickle("D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Datasets\\EMNIST_16x16_uppercase_Impl_SynContColor_hsv_Blackbckgrnd_InclLabels_2020-06-04.pkl")

utils.plot_data_set_distribution(train_dataset, False)
utils.plot_data_set_distribution(test_dataset, True)


# --- delta E2000 test:
# test_image_batch = utils.get_single_random_batch(batched_test_dataset)
# print("delta E CIE2000 - Test value:", utils.calculate_avg_across_batch_delta_e(test_image_batch,
#                                                                                 test_image_batch, data_shape))

# ---- Select random sample batch from training dataset to check for (conversion) errors

# batch = utils.get_single_random_batch(batched_train_dataset_cnn)
# _, color_images = utils.extract_original_images_in_dimension(batch, data_shape)
#
# if color_images.shape[1] == 2:
#     color_images = color_images[:, 0]
#
# # ---- Plot training verification batch
# if loader.lab and color_images is not None:
#     utils.plot_lab_batch_layers(color_images)
# elif loader.hsv and color_images is not None:
#     utils.plot_ver_hsv_batch(color_images)
# else:
#     utils.plot_ver_rgb_batch(color_images)

# ---- If everything is okay, by manually checking, proceed to GAN and/or CNN.

# ver_batch = utils.get_single_random_batch(BatchGenerator(256).get_batched_dataset(train_dataset))
#
# ver_images_set, _ = utils.extract_original_images_in_dimension(ver_batch, data_shape)
# ver_grey_images, _ = utils.extract_images_and_labels_from_batch(ver_images_set)
#
# utils.plot_ver_grey_batch(ver_grey_images, data_shape)

# ---- If everything is okay, by manually checking, proceed to GAN and/or CNN.

ver_batch = utils.get_single_random_batch(BatchGenerator(256).get_batched_dataset(train_dataset))
_, ver_images_set = utils.extract_original_images_in_dimension(ver_batch, data_shape)
ver_color_images, _ = utils.extract_images_and_labels_from_batch(ver_images_set)

utils.plot_ver_hsv_batch(ver_color_images)


CNN = CNN(input_shape=list(data_shape))
CNN.train(BatchGenerator(256).get_batched_dataset(train_dataset), 200,
          BatchGenerator(256).get_batched_dataset(test_dataset), 25)

GAN = GAN(input_shape=list(data_shape))
GAN.train(BatchGenerator(256).get_batched_dataset(train_dataset), 200,
          BatchGenerator(256).get_batched_dataset(test_dataset), 25)



# GAN.checkpoint.restore(tf.train.latest_checkpoint(GAN.checkpoint_dir))
# GAN.display_image(1)

# gif_creator = gif_creator("/Synesthesia_GAN/Data/test_images_3", "/Synesthesia_GAN/Data/progress_gifs")
# gif_creator.create_and_store_gif()
