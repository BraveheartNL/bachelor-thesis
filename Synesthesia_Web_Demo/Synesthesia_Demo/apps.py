from django.apps import AppConfig


class SynesthesiaDemoConfig(AppConfig):
    name = 'Synesthesia_Demo'
