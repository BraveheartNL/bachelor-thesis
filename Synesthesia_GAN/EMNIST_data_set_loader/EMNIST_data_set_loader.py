import os
import time
from datetime import date
from enum import Enum
from emnist import extract_training_samples
from emnist import extract_test_samples
import numpy as np
import string
import re
import scipy.ndimage
import tensorflow as tf
import pickle as pkl
from skimage import color
from colorsys import hsv_to_rgb
from colorsys import rgb_to_hsv
from Synesthesia_GAN.EMNIST_data_set_loader.EMNIST_label_creator import EMNIST_label_creator, LabelType


class UsageType(Enum):
    all = 1
    digits = 2
    uppercase = 3
    lowercase = 4


class EMNISTLoader(object):

    def __init__(self):
        self.__label_map = list(string.digits) + list(string.ascii_uppercase) + list(string.ascii_lowercase)
        self.batch_size = 256
        self.lab = False
        self.hsv = False
        self.__grey_rgb = np.repeat(197 / 255, 3)

    def get_emnist_database(self, usage: UsageType, image_size: str = '16x16', explicit_labels: bool = False,
                            convert_to_synesthetic_color: bool = False, batch_size: int = 256,
                            continuous_color: bool = True, color_map_conversion: str = "lab", store_pkl: bool = True,
                            grey_background: bool = False, merge_labels_with_datasets: bool = False):
        # ------------------------------------------ User input verification -------------------------------------------
        self.batch_size = batch_size
        self.color_map_conversion = color_map_conversion
        size_regex = re.compile(r'\d+x\d+', re.IGNORECASE)
        size = size_regex.match(image_size).group(0)
        if size:
            split_size = size.split('x')
            if split_size[0] != split_size[1]:
                raise ValueError("Database picture dimensions must be of format \"d+xd+\" and first and second digit(s)"
                                 "must be equal! (maintain aspect ratio)!")
        else:
            raise ValueError("Database picture dimensions must be of format \"d+xd+\"!")

        # if convert_to_synesthetic_rgb:
        #     grey_background = True

        # ----------------------------------------- Reshaping dataset to user dimensions -------------------------------
        print("----------------------- FETCHING DATASET -----------------------")

        grey_train_images, train_labels, grey_test_images, test_labels = self.__get_emnist_datasets(usage)
        color_test_images = color_train_images = None
        print("------------------------ PRE-PROCESSING ------------------------")

        print("{} - Start reshaping of dataset from 28x28 to {}....".format(self.__class__.__name__, image_size))

        grey_train_images = grey_train_images.reshape(grey_train_images.shape[0], 28, 28, 1)
        grey_test_images = grey_test_images.reshape(grey_test_images.shape[0], 28, 28, 1)

        split_size_int: [int] = [int(item) for item in size.split('x')]
        reshape_factor = round(split_size_int[0] / 28, 2)

        start = time.time()
        grey_train_images = np.asarray(
            [scipy.ndimage.zoom(image, (reshape_factor, reshape_factor, 1), order=1) for image in grey_train_images])
        print("{} - Done reshaping training-set according to user specifications in {:.2f} seconds!".format(
            self.__class__.__name__,
            time.time() - start))
        start = time.time()
        grey_test_images = np.asarray(
            [scipy.ndimage.zoom(image, (reshape_factor, reshape_factor, 1), order=1) for image in grey_test_images])
        print("{} - Done reshaping test-set according to user specifications in {:.2f} seconds!".format(
            self.__class__.__name__,
            time.time() - start))

        # ------------------------------------------ Explicit Label Creation    ----------------------------------------
        label_creator = EMNIST_label_creator()
        if explicit_labels:
            label_creator.create_labels(LabelType.explicit, dict(test_labels=test_labels, train_labels=train_labels))
        if convert_to_synesthetic_color:
            label_creator.create_labels(LabelType.explicit_and_synesthetic,
                                        dict(test_labels=test_labels, train_labels=train_labels))

        # ------------------------------------------ Grayscale-2-RGB Conversion ----------------------------------------

        if not convert_to_synesthetic_color and continuous_color:
            print(
                "{} - Starting Greyscale-2-ContinuousRGB conversion of dataset...".format(self.__class__.__name__))
            start = time.time()

            color_test_images, color_train_images = self.__convert_dataset_to_rgb(grey_background, grey_test_images,
                                                                                  grey_train_images,
                                                                                  rgb_random=True)

            print("{} - Greyscale-2-ContinuousRGB conversion of dataset Done in {:.2f} seconds!".format(
                self.__class__.__name__,
                time.time() - start))
            print("")

        # Non-continous random RGB encodig:
        if not convert_to_synesthetic_color and not continuous_color:
            print(
                "{} - Starting Greyscale-2-FlatRGB conversion of dataset...".format(self.__class__.__name__))
            start = time.time()

            color_test_images, color_train_images = self.__convert_dataset_to_random_non_continuous_rgb(grey_background,
                                                                                                        grey_test_images,
                                                                                                        grey_train_images)

            print("{} - Greyscale-2-FlatRGB conversion of dataset done in {:.2f} seconds!".format(
                self.__class__.__name__,
                time.time() - start))
            print("")

        if convert_to_synesthetic_color and not continuous_color:
            raise NotImplementedError("Conversion to synesthetic rgb and continous color is not yet implemented.")

        if convert_to_synesthetic_color and continuous_color:
            print(
                "{} - Starting Greyscale-2-ContinousRGB synesthetic conversion of dataset...".format(
                    self.__class__.__name__))
            start = time.time()

            color_test_images, color_train_images = self.__convert_dataset_to_rgb(grey_background, grey_test_images,
                                                                                  grey_train_images,
                                                                                  rgb_random=False,
                                                                                  label_creator=label_creator)

            print("{} - Greyscale-2-ContinuousRGB synesthetic conversion of dataset done in {:.2f} seconds!".format(
                self.__class__.__name__,
                time.time() - start))
            print("")

        # ------------------------------------------ RGB-to-CIELAB Conversion ------------------------------------------
        if color_map_conversion == "lab" and color_train_images is not None and color_test_images is not None:
            self.lab = True
            print(
                "{} - Starting RGB-2-CIELAB conversion of dataset...".format(self.__class__.__name__))
            start = time.time()
            color_train_images = np.asarray([color.rgb2lab(image) for image in color_train_images])
            color_test_images = np.asarray([color.rgb2lab(image) for image in color_test_images])
            print("{} - RGB-2-CIELAB conversion of dataset done in {:.2f} seconds!".format(
                self.__class__.__name__,
                time.time() - start))
            print("")
        elif color_map_conversion == "hsv" and color_train_images is not None and color_test_images is not None:
            self.hsv = True
            print(
                "{} - Starting RGB-2-HSV conversion of dataset...".format(self.__class__.__name__))
            start = time.time()
            color_train_images = np.asarray([color.rgb2hsv(image) for image in color_train_images])
            color_test_images = np.asarray([color.rgb2hsv(image) for image in color_test_images])
            print("{} - RGB-2-HSV conversion of dataset done in {:.2f} seconds!".format(
                self.__class__.__name__,
                time.time() - start))
            print("")
        else:
            raise ValueError("color_map_conversion parameter should be either \'lab\' or \'hsv\'!")

        # ------------------------------------------ Packaging of Dataset ----------------------------------------------
        test_dataset = None
        train_dataset = None

        if color_train_images is not None and color_test_images is not None:
            print(
                "{} - Starting packaging of grey scale and color images into dataset...".format(
                    self.__class__.__name__))
            start = time.time()

            if (grey_train_images.shape[0] == color_train_images.shape[0]
                    and grey_test_images.shape[0] == color_test_images.shape[0]):
                if not merge_labels_with_datasets:
                    data_shape = (grey_train_images.shape[1], grey_train_images.shape[2])
                    train_images = np.asarray([[np.ravel(grey_train_images[i]), np.ravel(color_train_images[i])] for i in
                                               range(grey_train_images.shape[0])])
                    test_images = np.asarray([[np.ravel(grey_test_images[i]), np.ravel(color_test_images[i])] for i in
                                              range(grey_test_images.shape[0])])
                if merge_labels_with_datasets and convert_to_synesthetic_color:
                    data_shape = (grey_train_images.shape[1], grey_train_images.shape[2])
                    train_dataset = np.asarray(
                        [[np.ravel(grey_train_images[i]), np.ravel(color_train_images[i]),
                          label_creator.synesthetic_dataset_labels.get('train_labels')[i]] for i in
                         range(grey_train_images.shape[0])])
                    test_dataset = np.asarray([[np.ravel(grey_test_images[i]), np.ravel(color_test_images[i]),
                                                label_creator.synesthetic_dataset_labels.get('test_labels')[i]] for i in
                                               range(grey_test_images.shape[0])])
            else:
                raise ValueError("Something went wrong while converting greyscale images to color images! "
                                 "Grey and Color image sets do not have the same shape!")
            print(
                "{} - Packaging of grey scale and color images into dataset done in {:.2f} seconds!".
                    format(self.__class__.__name__, time.time() - start))
        else:
            data_shape = (grey_train_images.shape[1], grey_train_images.shape[2])
            train_images = np.asarray(grey_train_images)
            test_images = np.asarray(grey_test_images)

        # ------------------------------------------ Build pickle string and save --------------------------------------

        if store_pkl:
            print("------------------------ SAVING DATASET ------------------------")
            print(
                "{} - Storing dataset as Pickle file".format(self.__class__.__name__))

            if merge_labels_with_datasets:
                pickle_string = self.__store_dataset_as_pickle(continuous_color, convert_to_synesthetic_color,
                                                               explicit_labels,
                                                               grey_background,
                                                               image_size, color_map_conversion, usage, data_shape,
                                                               merge_labels_with_datasets,
                                                               test_data_set=test_dataset, train_data_set=train_dataset
                                                               )
            else:
                pickle_string = self.__store_dataset_as_pickle(continuous_color, convert_to_synesthetic_color,
                                                               explicit_labels,
                                                               grey_background,
                                                               image_size, color_map_conversion, usage, data_shape,
                                                               merge_labels_with_datasets,
                                                               test_images=test_images, train_images=train_images,
                                                               test_labels=test_labels, train_labels=train_labels)

            print(
                "{} - Stored dataset as pickle file under name: {}".format(self.__class__.__name__, pickle_string))
            print("---------------------- PRE-PROCESSING DONE ---------------------")

        if merge_labels_with_datasets:
            return train_dataset, test_dataset, data_shape
        else:
            return train_images, train_labels, test_images, test_labels, data_shape

    # -------------------------------------------- Class Methods -------------------------------------------------------

    # -------------------------------------------- Public Class Methods ------------------------------------------------
    def load_dataset_from_pickle(self, path: str):
        print("------------------------ LOADING DATASET ------------------------")
        print("{} - retrieving EMNIST dataset from user designated pickle string: {}".format(self.__class__.__name__,
                                                                                             path))
        if "_lab_" in path:
            self.lab = True
        else:
            self.lab = False

        if "_hsv_" in path:
            self.hsv = True
        else:
            self.hsv = False

        if "ExclLabels_" in path:
            with open(path, "rb") as f:
                train_images, train_labels, test_images, test_labels, data_shape = pkl.load(f)
        else:
            with open(path, "rb") as f:
                train_dataset, test_dataset, data_shape = pkl.load(f)

        print("{} - Done EMNIST dataset from user designated pickle string".format(self.__class__.__name__))
        print("---------------------- DONE LOADING DATASET ----------------------")

        if "_Excl_labels" in path:
            return train_images, train_labels, test_images, test_labels, data_shape
        else:
            return train_dataset, test_dataset, data_shape

    # -------------------------------------------- Private Class Methods -----------------------------------------------

    def __store_dataset_as_pickle(self, continuous_color, convert_to_synesthetic_color, explicit_labels,
                                  grey_background,
                                  image_size, color_map_conversion, usage, data_shape, merged_labels_with_dataset,
                                  train_data_set=None, test_data_set=None, test_images=None, test_labels=None,
                                  train_images=None, train_labels=None):

        explicit_labels_str = "Expl" if explicit_labels else "Impl"
        merged_labels_with_dataset_str = ""
        if merged_labels_with_dataset:
            merged_labels_with_dataset_str = "InclLabels_" if merged_labels_with_dataset else "ExclLabels_"
        grey_background = "Greybckgrnd" if grey_background else "Blackbckgrnd"
        continuous_color_str = "Cont" if continuous_color else "Flat"
        color_str = "Syn{}Color".format(
            continuous_color_str) if convert_to_synesthetic_color else "Rand{}Color".format(continuous_color_str)
        pickle_string = "\\Datasets\\EMNIST_{}_{}_{}_{}_{}_{}_{}{}.pkl".format(
            image_size, str(usage.name), explicit_labels_str, color_str, color_map_conversion, grey_background,
            merged_labels_with_dataset_str, date.today())
        if merged_labels_with_dataset:
            with open(os.getcwd() + pickle_string, mode="wb") as f:
                pkl.dump([train_data_set, test_data_set, data_shape], f)
        else:
            with open(os.getcwd() + pickle_string, mode="wb") as f:
                pkl.dump([train_images, train_labels, test_images, test_labels, data_shape], f)

        return pickle_string

    def __convert_dataset_to_random_non_continuous_rgb(self, grey_background, test_images, train_images):
        train_images = train_images / 255
        test_images = test_images / 255
        train_images = self.greyscale_2_noncontinuous_rgb(train_images)
        train_images = np.concatenate([train_images, train_images, train_images], axis=3)
        test_images = self.greyscale_2_noncontinuous_rgb(test_images)
        test_images = np.concatenate([test_images, test_images, test_images], axis=3)
        if grey_background:  # Apply Boolean mask along the color axis, where all three RGB values are 0
            train_images = self.convert_image_set_to_grey_background(train_images)
            test_images = self.convert_image_set_to_grey_background(train_images)
        return test_images, train_images

    def greyscale_2_noncontinuous_rgb(self, image_set):
        for i in range(image_set.shape[0]):
            rgb = np.random.choice(range(255))
            color_channel = image_set[i, :, :, 0]
            color_channel = np.asarray([[(int(c * rgb) / 255) for c in cr] for cr in color_channel])
            image_set[i, :, :, 0] = color_channel
        return image_set

    def __convert_dataset_to_rgb(self, grey_background, test_images, train_images, rgb_random: bool, label_creator):
        train_images = train_images / 255
        test_images = test_images / 255
        train_images = np.concatenate([train_images, train_images, train_images], axis=3)
        test_images = np.concatenate([test_images, test_images, test_images], axis=3)
        largest_dataset_size = train_images.shape[0] if train_images.shape[0] > test_images.shape[0] else \
            test_images.shape[0]
        train_done = False
        test_done = False
        progress = [int(largest_dataset_size * 0.25), int(largest_dataset_size * 0.5), int(largest_dataset_size * 0.75)]

        for i in range(largest_dataset_size):
            if i in progress:
                print(
                    "{} - Greyscale to RGB conversion in progress - {:.2f}% done...".format(self.__class__.__name__, \
                                                                                            float((
                                                                                                          i / largest_dataset_size
                                                                                                  ) * 100)))
            train_image = None
            test_image = None
            if (i <= len(train_images) - 1) and (i <= len(test_images) - 1):
                train_image = train_images[i]
                test_image = test_images[i]
            elif (i <= len(train_images) - 1) and not (i <= len(test_images) - 1):
                train_image = train_images[i]
                test_done = True
            else:
                test_image = test_images[i]
                train_done = True

            if rgb_random:
                rgb_train = list(np.random.choice(range(255), size=3))
                rgb_test = list(np.random.choice(range(255), size=3))
            else:
                syn_color_train_labels = label_creator.synesthetic_dataset_labels.get("train_labels")
                syn_color_test_labels = label_creator.synesthetic_dataset_labels.get("test_labels")
                # train_label = syn_color_train_labels[i][0]
                rgb_test = False
                rgb_train = None
                if not train_done:
                    train_image = train_images[i]
                    syn_color_hsv_train = syn_color_train_labels[i][-1]
                    rgb = list(hsv_to_rgb(syn_color_hsv_train.get("h"), syn_color_hsv_train.get("s"),
                                          syn_color_hsv_train.get("v")))
                    rgb_train = [int(x * 255) for x in rgb]
                if not test_done:
                    test_image = test_images[i]
                    syn_color_hsv_test = syn_color_test_labels[i][-1]
                    rgb = list(hsv_to_rgb(syn_color_hsv_test.get("h"), syn_color_hsv_test.get("s"),
                                          syn_color_hsv_test.get("v")))
                    rgb_test = [int(x * 255) for x in rgb]

            images = self.__greyscale_2_rgb(dict(train_image=train_image, test_image=test_image),
                                            rgb_train=rgb_train, rgb_test=rgb_test, grey_background=grey_background)

            train_image = images.get("train_image")
            test_image = images.get("test_image")
            if train_image is not None and test_image is not None:
                train_images[i] = train_image
                test_images[i] = test_image
            elif test_image is None:
                train_images[i] = train_image
            else:
                test_images[i] = test_image
        return test_images, train_images

    def __get_emnist_datasets(self, usage: UsageType):
        print("{} - retrieving EMNIST dataset from API following user specified parameters....".format(
            self.__class__.__name__))
        start = time.time()

        # create label map - 0-9 = digits. 10-35 = uppercase letters, 36-61= lowercase letters.
        if usage == UsageType.digits:
            training_images, training_labels = extract_training_samples(dataset="digits")
            test_images, test_labels = extract_test_samples(dataset="digits")
        else:
            training_images, training_labels = extract_training_samples(dataset="byclass")
            test_images, test_labels = extract_test_samples(dataset="byclass")
            if usage == UsageType.uppercase:
                training_index, = np.where((training_labels < 10) | (training_labels > 35))
                test_index, = np.where((test_labels < 10) | (test_labels > 35))
                training_images = np.delete(training_images, training_index, axis=0)
                training_labels = np.delete(training_labels, training_index)
                test_labels = np.delete(test_labels, test_index)
                test_images = np.delete(test_images, test_index, axis=0)
            elif usage == UsageType.lowercase:
                training_index = np.where((training_labels < 36) | (training_labels > 61))
                test_index, = np.where((test_labels < 36) | (test_labels > 61))
                training_images = np.delete(training_images, training_index, axis=0)
                training_labels = np.delete(training_labels, training_index)
                test_labels = np.delete(test_labels, test_index)
                test_images = np.delete(test_images, test_index, axis=0)

        print("{} - Done fetching dataset from API in {:.2f} seconds!".format(self.__class__.__name__,
                                                                              time.time() - start))

        return training_images, training_labels, test_images, test_labels

    # -------------------------------------------- HSV MAP -------------------------------------------------------------

    # HSV - S=uniform(0.2,1), V = uniform(0.3,1).
    # RED   : A, M, R H = 0, H=(range(0.958-1))
    # Blue  : B, D, T, W - H=(range(0.667-0.75))
    # Green : E, F, G - H = range(0.25,0.38)
    # Yellow: C, L, S, Y - H = range(0.167-0.181)
    # Orange: H, J, K, N, U - H = range(0.056 - 0.0834)
    # Purple: P, Q, V - H = range(0.778 - 0.834)
    # White : I, O - HSV=(0,0,range(0.85-1))
    # Black : X, Z - HSV=(0,0,range(0-0.15))
    # x = 1/360 = 0,00277777777777777777777777777778 -> float conversion of 360 degrees HSV wheel
    # hue float value needed = Hue degrees * x.

    def __greyscale_2_rgb(self, images: dict, rgb_train, rgb_test, grey_background: bool):
        train_image = images.get("train_image")
        test_image = images.get("test_image")
        images = [train_image, test_image]
        idx = 0
        for image in images:
            if image is not None and rgb_train is not None and rgb_test is not None:
                rgb = rgb_train if idx == 0 else rgb_test
                for j in range(3):
                    color_channel = image[:, :, j]
                    color_channel = np.asarray([[(int(c * rgb[j]) / 255) for c in cr] for cr in color_channel])
                    image[:, :, j] = color_channel
                if grey_background:  # Apply Boolean mask along the color axis, where all three RGB values are 0.
                    black_backgrnd_idxs = np.all(image == np.zeros(3), axis=-1)
                    image[black_backgrnd_idxs, :] = self.__grey_rgb  # Replace boolean mask with grey color.
                idx += 1
            else:
                idx += 1

        return dict(train_image=images[0], test_image=images[1])

    def convert_image_set_to_grey_background(self, image_set):
        for i in range(image_set.shape[0]):
            train_image = image_set[i]
            black_backgrnd_idxs = np.all(train_image == np.zeros(3), axis=-1)
            train_image[black_backgrnd_idxs, :] = self.__grey_rgb  # Replace boolean mask with grey color.
            image_set[i] = train_image
        return image_set
