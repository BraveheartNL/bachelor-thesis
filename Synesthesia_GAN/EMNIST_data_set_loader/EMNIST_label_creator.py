import string
import time

import numpy as np
from enum import Enum

from Synesthesia_GAN.synesthetic_color_letter_mapper.synesthetic_color_letter_mapper import \
    Synesthetic_color_letter_mapper


class LabelType(Enum):
    explicit = 1
    synesthetic = 2
    explicit_and_synesthetic = 3


class EMNIST_label_creator(object):

    def __init__(self):
        self.explicit_dataset_labels = {}
        self.synesthetic_dataset_labels = {}
        self.__label_map = list(string.digits) + list(string.ascii_uppercase) + list(string.ascii_lowercase)
        self.__synesthetic_color_letter_map = {}

    def create_labels(self, label_type: LabelType, data_set_labels: dict):
        train_labels = data_set_labels.get("train_labels")
        test_labels = data_set_labels.get("test_labels")

        if test_labels is None or train_labels is None:
            raise ValueError("Emnist_label_creator method \"create_labels\", requires dictionary parameter "
                             "\"data_set_labels\" to contain \"test_labels\"-key and \"train_labels\"-key, "
                             "with dataset labels!")

        if type(test_labels) is not np.ndarray or type(train_labels) is not np.ndarray:
            raise ValueError("Emnist_label_creator method \"create_labels\", requires dictionary parameter "
                             "\"data_set_labels\" to contain \"test_labels\"-key and \"train_labels\"-key values to "
                             "be numpy arrays of char type labels!")

        if label_type is LabelType.explicit:
            self.__create_explicit_labels(test_labels, train_labels)
            print("")
        elif label_type is LabelType.synesthetic:
            self.__create_synesthetic_labels(test_labels, train_labels)
            print("")
        elif label_type is LabelType.explicit_and_synesthetic:
            self.__create_explicit_labels(test_labels, train_labels)
            self.__create_synesthetic_labels(test_labels, train_labels)
            print("")
        else:
            raise ValueError("EMNIST_label_creator needs a valid label_type parameter of type LabelType (Enum)!")

    def __create_explicit_labels(self, test_labels: np.ndarray, train_labels: np.ndarray):
        print("Emnist_label_creator - Creating Explicit labels for dataset....")
        start = time.time()
        train_labels = np.asarray([self.__label_map[x] for x in train_labels])
        test_labels = np.asarray([self.__label_map[x] for x in test_labels])
        self.explicit_dataset_labels.update(test_labels=test_labels, train_labels=train_labels)
        self.explicit_dataset_labels.update(train_labels=train_labels)
        print("Emnist_label_creator - Done creating Explicit labels for dataset in {:.2f} seconds!".format(
            time.time() - start))

    def __create_synesthetic_labels(self, test_labels: np.ndarray, train_labels: np.ndarray):
        print("Emnist_label_creator - Creating Synesthetic labels for dataset....")
        start = time.time()
        syn_col_letter_map = Synesthetic_color_letter_mapper()
        self.__synesthetic_color_letter_map = syn_col_letter_map.color_letter_map

        # Format:
        # label = (original_label:int, explicit_label:char, letter_synesthetic_color: SynestheticLetterColorMap,
        # hsv_values: dict(h:float, s:float, v:float))

        train_labels = np.asarray([[x, self.__label_map[x],
                                    syn_col_letter_map.get_char_synesthetic_color_name(train_labels[x]),
                                    syn_col_letter_map.get_char_synesthetic_color_hsv_values(train_labels[x])] for x
                                   in train_labels])
        test_labels = np.asarray([[x, self.__label_map[x],
                                   syn_col_letter_map.get_char_synesthetic_color_name(test_labels[x]),
                                   syn_col_letter_map.get_char_synesthetic_color_hsv_values(test_labels[x])] for x
                                  in test_labels])

        self.synesthetic_dataset_labels.update(test_labels=test_labels, train_labels=train_labels)
        # self.synesthetic_dataset_labels.update(train_labels=train_labels)

        print("Emnist_label_creator - Done creating Synesthetic labels for dataset in {:.2f} seconds!".format(
            time.time() - start))
