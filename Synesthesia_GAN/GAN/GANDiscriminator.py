import os

import tensorflow as tf
import tensorflow.keras.layers as layers


class GANDiscriminator(tf.keras.Sequential):

    def __init__(self, input_shape: [int]):
        self.checkpoint_dir = './GAN_training_checkpoints'
        self.checkpoint_prefix = os.path.join(self.checkpoint_dir, "ckpt")
        self.ndf = input_shape[0]

        super(GANDiscriminator, self).__init__()
        self.conv2d1 = layers.Conv2D(self.ndf, (5, 5), strides=(2, 2), padding='same', input_shape=input_shape)
        self.relu1 = layers.LeakyReLU()
        self.dropout1 = layers.Dropout(0.3)
        self.conv2d2 = layers.Conv2D(self.ndf * 2, (5, 5), strides=(2, 2), padding='same')
        self.relu2 = layers.LeakyReLU()
        self.dropout2 = layers.Dropout(0.3)
        self.flatten1 = layers.Flatten()
        self.dense1 = layers.Dense(1)

    def call(self, inputs, training=None, mask=None):
        x = self.conv2d1(inputs)
        x = self.relu1(x)
        x = self.dropout1(x)
        x = self.conv2d2(x)
        x = self.relu2(x)
        x = self.dropout2(x)
        x = self.flatten1(x)
        return self.dense1(x)

    def loss(self, real_output, fake_output):
        cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        return real_loss + fake_loss
