import time
import tensorflow as tf
import tensorflow.keras.layers as layers
import numpy as np

class GANGenerator(tf.keras.Sequential):

    def __init__(self, input_shape: [int]):

        super(GANGenerator, self).__init__()

        self.conv2d1 = layers.Conv2D(1, (5, 5), strides=1, padding='same')
        self.batch_norm1 = layers.BatchNormalization()
        self.relu1 = layers.LeakyReLU()
        self.conv2d2 = layers.Conv2D(2, (5, 5), strides=1, padding='same')
        self.batch_norm2 = layers.BatchNormalization()
        self.relu2 = layers.LeakyReLU()
        self.conv2d3 = layers.Conv2D(4, (5, 5), strides=1, padding='same')
        self.batch_norm3 = layers.BatchNormalization()
        self.relu3 = layers.LeakyReLU()
        self.conv2d4 = layers.Conv2D(8, (5, 5), strides=1, padding='same')
        self.batch_norm4 = layers.BatchNormalization()
        self.relu4 = layers.LeakyReLU()
        self.conv2d5 = layers.Conv2D(16, (5, 5), strides=1, padding='same')
        self.batch_norm5 = layers.BatchNormalization()
        self.relu5 = layers.LeakyReLU()
        self.conv2d6 = layers.Conv2D(32, (5, 5), strides=1, padding='same')
        self.batch_norm6 = layers.BatchNormalization()
        self.relu6 = layers.LeakyReLU()
        self.conv2d7 = layers.Conv2D(8, (5, 5), strides=1, padding='same')
        self.batch_norm7 = layers.BatchNormalization()
        self.relu7 = layers.LeakyReLU()
        self.conv2d8 = layers.Conv2D(2, (5, 5), strides=1, padding='same')
        self.batch_norm8 = layers.BatchNormalization()
        self.relu8 = layers.LeakyReLU()
        self.conv2d9 = layers.Conv2D(1, (5, 5), strides=1, padding='same')
        self.batch_norm9 = layers.BatchNormalization()
        self.relu9 = layers.LeakyReLU()
        self.flatten1 = layers.Flatten()
        self.dense1 = layers.Dense(512, use_bias=False, input_shape=(5, 5, 1))
        self.reshape1 = layers.Reshape((input_shape[0], input_shape[0], 2))

    def call(self, inputs, training=None, mask=None):
        start = time.time()

        x = self.conv2d1(inputs)
        x = self.batch_norm1(x)
        x = self.relu1(x)

        x = self.conv2d2(x)
        x = self.batch_norm2(x)
        x = self.relu2(x)

        x = self.conv2d3(x)
        x = self.batch_norm3(x)
        x = self.relu3(x)

        x = self.conv2d4(x)
        x = self.batch_norm4(x)
        x = self.relu4(x)

        x = self.conv2d5(x)
        x = self.batch_norm5(x)
        x = self.relu5(x)

        x = self.conv2d6(x)
        x = self.batch_norm6(x)
        x = self.relu6(x)

        x = self.conv2d7(x)
        x = self.batch_norm7(x)
        x = self.relu7(x)

        x = self.conv2d8(x)
        x = self.batch_norm8(x)
        x = self.relu8(x)

        x = self.conv2d9(x)
        x = self.batch_norm9(x)
        x = self.relu9(x)

        x = self.flatten1(x)
        x = self.dense1(x)
        x = self.reshape1(x)

        return tf.concat([tf.dtypes.cast(tf.expand_dims(x[:, :, :, 0], 3), tf.float64),
                          tf.ones(list(inputs.shape)[0:4], dtype=tf.float64),
                          tf.dtypes.cast(tf.expand_dims(x[:, :, :, 1], 3), dtype=tf.float64)], axis=3)
        # return tf.concat([tf.dtypes.cast(x, dtype=tf.float64),
        #                   tf.dtypes.cast(tf.ones(list(inputs.shape)[0:4]), dtype=tf.float64)], axis=3)

    def loss(self, real_output, fake_output):
        return tf.keras.losses.MeanSquaredError().call(y_true=real_output, y_pred=fake_output)

