import os
import string
import time
from Synesthesia_GAN.interfaces.ISynestheticNeuralNet import ISynestheticNeuralNet
from IPython import display
from Synesthesia_GAN.GAN.GANDiscriminator import GANDiscriminator
from Synesthesia_GAN.GAN.GANGenerator import GANGenerator
import tensorflow as tf
import random
import numpy as np
import Synesthesia_GAN.Utilities as utils


class GAN(ISynestheticNeuralNet, object):

    def __init__(self, input_shape: [int]):
        print("---------------------- INITIALIZING GAN ---------------------")

        self.input_shape = input_shape
        self.GANGenerator = GANGenerator(input_shape=input_shape)
        self.GANDiscriminator = GANDiscriminator(input_shape=input_shape)

        self.generator_optimizer = tf.keras.optimizers.Adam(1e-3)
        self.discriminator_optimizer = tf.keras.optimizers.Adam(1e-3)

        self.checkpoint_dir = './Training_checkpoints/GAN_training_checkpoints'
        self.checkpoint_prefix = os.path.join(self.checkpoint_dir, "ckpt")
        self.checkpoint = tf.train.Checkpoint(generator_optimizer=self.generator_optimizer,
                                              discriminator_optimizer=self.discriminator_optimizer,
                                              generator=self.GANGenerator,
                                              discriminator=self.GANDiscriminator)

        # self.seed = tf.random.normal([16, 100])
        print("------------------- INITIALIZING GAN DONE -------------------")

    @tf.function
    def train_step(self, grey_images, color_images):
        # noise = tf.random.normal([256, 100])

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_output = self.GANGenerator(grey_images, training=True)

            real_output = self.GANDiscriminator(color_images, training=True)
            fake_output = self.GANDiscriminator(generated_output, training=True)

            gen_loss = self.GANGenerator.loss(color_images, generated_output)
            disc_loss = self.GANDiscriminator.loss(real_output, fake_output)

        gradients_of_generator = gen_tape.gradient(gen_loss, self.GANGenerator.trainable_variables)
        gradients_of_discriminator = disc_tape.gradient(disc_loss, self.GANDiscriminator.trainable_variables)

        self.generator_optimizer.apply_gradients(zip(gradients_of_generator, self.GANGenerator.trainable_variables))
        self.discriminator_optimizer.apply_gradients(
            zip(gradients_of_discriminator, self.GANDiscriminator.trainable_variables))

        return gen_loss, disc_loss, generated_output

    def train(self, train_dataset, epochs, test_dataset, no_of_batches_per_epoch):
        train_session_test_delta_hsv_per_letter = {}
        train_session_test_delta_hsv_per_color = {}
        train_session_test_delta_hsv = []
        train_session_train_disc_loss = []
        train_session_train_gen_loss = []

        print("{} - Start training network and evaluating within epochs....".format(
            self.__class__.__name__))

        for epoch in range(epochs):
            start = time.time()
            epoch_gen_loss = []
            epoch_disc_loss = []

            i = no_of_batches_per_epoch
            while i != 0:
                image_batch = next(train_dataset)
                grey_train_batch, color_train_batch = utils.extract_original_images_in_dimension(image_batch,
                                                                                                 self.input_shape)

                grey_train_images, grey_train_labels = utils.extract_images_and_labels_from_batch(grey_train_batch)
                color_train_images, color_train_labels = utils.extract_images_and_labels_from_batch(color_train_batch)

                grey_train_images = tf.convert_to_tensor([image * (1.0 / 255.0) for image in grey_train_images],
                                                         dtype=tf.float64)

                color_train_images = tf.convert_to_tensor(color_train_images.tolist())

                gan_out = self.train_step(grey_train_images, color_train_images)

                # (generator_loss, discriminator_loss, generated_images)
                epoch_gen_loss.append(np.average(gan_out[0].numpy()))
                epoch_disc_loss.append(np.average(gan_out[1].numpy()))

                i -= 1

            # Produce images for the GIF as we go
            display.clear_output(wait=True)

            delta_hsvs = self.generate_and_save_images(self.GANGenerator, epoch + 1,
                                                       utils.get_single_random_batch(test_dataset))

            train_session_test_delta_hsv.append(delta_hsvs[2])

            # update all error containers with epoch values.
            for key in delta_hsvs[0]:
                if isinstance(train_session_test_delta_hsv_per_letter.get(key), list) \
                        and bool(train_session_test_delta_hsv_per_letter.get(key)) and delta_hsvs[0].get(key):
                    train_session_test_delta_hsv_per_letter.update(
                        {key: train_session_test_delta_hsv_per_letter.get(key)
                                + [delta_hsvs[0].get(key)]})
                else:
                    if delta_hsvs[0].get(key):
                        train_session_test_delta_hsv_per_letter.update({key: [delta_hsvs[0].get(key)]})

            for key in delta_hsvs[1]:
                if isinstance(train_session_test_delta_hsv_per_color.get(key), list) \
                        and bool(train_session_test_delta_hsv_per_color.get(key)) and delta_hsvs[1].get(key):
                    train_session_test_delta_hsv_per_color.update(
                        {key: train_session_test_delta_hsv_per_color.get(key)
                                + [delta_hsvs[1].get(key)]})
                else:
                    if delta_hsvs[1].get(key):
                        train_session_test_delta_hsv_per_color.update({key: [delta_hsvs[1].get(key)]})

            train_session_train_gen_loss.append(np.average(epoch_gen_loss))
            train_session_train_disc_loss.append(np.average(epoch_disc_loss))

            # Save the model every 15 epochs
            if (epoch + 1) % 15 == 0:
                self.checkpoint.save(file_prefix=self.checkpoint_prefix)

            print('Time for epoch {} is {:.2f} sec'.format(epoch + 1, time.time() - start))

        # Generate after the final epoch
        display.clear_output(wait=True)
        self.generate_and_save_images(self.GANGenerator, epochs, utils.get_single_random_batch(test_dataset))

        print("{} - Plotting evaluation measures....".format(self.__class__.__name__))
        self.plot_train_session_test_hsv_distance_and_train_loss(train_session_test_delta_hsv,
                                                                 (train_session_train_gen_loss,
                                                                  train_session_train_disc_loss),
                                                                  train_session_test_delta_hsv_per_letter,
                                                                  train_session_test_delta_hsv_per_color,
                                                                 "L2 Loss")

        print("{} - Done training network and evaluating within epochs!".format(
            self.__class__.__name__))
