import abc
import string
import logging
import tensorflow as tf
from colormath.color_objects import HSVColor
from colormath.color_objects import LabColor

from colormath.color_diff import delta_e_cie2000
import matplotlib.pyplot as plt
import PIL.Image
import numpy as np
from skimage import color
import math
from matplotlib.colors import hsv_to_rgb
import Synesthesia_GAN.Utilities as utils


class ISynestheticNeuralNet(object, metaclass=abc.ABCMeta):

    def __init(self, input_shape: [int]):
        self.input_shape = input_shape

    @abc.abstractmethod
    def train_step(self, grey_image_batch, color_image_batch):
        raise NotImplementedError("User of this abstract class must implement this method.")

    @abc.abstractmethod
    def train(self, train_dataset, epochs, test_dataset, no_of_batches_per_epoch):
        raise NotImplementedError("User of this abstract class must implement this method.")

    def generate_and_save_images(self, model, epoch, test_batch):
        grey_test_batch_data, color_test_batch_data = utils.extract_original_images_in_dimension(test_batch,
                                                                                                 self.input_shape)
        # remove test labels from merged data:
        grey_test_images, grey_test_labels = utils.extract_images_and_labels_from_batch(grey_test_batch_data)
        color_test_images, color_test_labels = utils.extract_images_and_labels_from_batch(color_test_batch_data)

        grey_test_images = tf.convert_to_tensor([image / 255 for image in grey_test_images], dtype=tf.float64)
        # color_test_images = tf.convert_to_tensor(color_test_images.tolist())

        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        generated_images = model(grey_test_images, training=False)

        # merge images with the original color labels, representing the image it should be and the color it should have.
        generated_data = utils.merge_image_batch_with_label_batch(generated_images.numpy(), color_test_labels)

        epoch_delta_hsvs = self.calculate_epoch_delta_hsv(color_test_batch_data, generated_data)

        predictions_rgb_dec = np.asarray([hsv_to_rgb(image) for image in generated_images.numpy()])
        plt.figure(figsize=(5, 5))

        logger = logging.getLogger()
        old_level = logger.level
        logger.setLevel(100)

        for i in range(25):
            plt.subplot(5, 5, i + 1)
            plt.imshow(predictions_rgb_dec[i])
            plt.axis('off')

        logger.setLevel(old_level)

        plt.savefig(
            'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_test_image_epoch_{:04d}.png'.format(
                self.__class__.__name__,
                epoch))

        plt.show()

        return epoch_delta_hsvs

    def display_image(self, epoch):
        return PIL.Image.open(
            'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_test_image_epoch_{:04d}.png'.format(
                self.__class__.__name__,
                epoch))

    def calculate_epoch_delta_hsv(self, test_image_data: np.ndarray, generated_image_data: np.ndarray):
        delta_hsv_per_letter = dict.fromkeys(string.ascii_uppercase)
        delta_hsv_per_color = dict.fromkeys([test_image_data[i][1][2] for i in range(test_image_data.shape[0] - 1)])
        batch_avg_delta_hsv = []

        i = 0
        for test_image_label_pair in test_image_data:
            gen_image_label = generated_image_data[i][1][1]
            test_image_label = test_image_label_pair[1][1]

            gen_image_color_label = generated_image_data[i][1][2]
            test_image_color_label = test_image_label_pair[1][2]

            if gen_image_label != test_image_label:
                raise ValueError("To calculate delta HSV across test and generated images,"
                                 " both images must be concern the same letter!")
            elif gen_image_color_label != test_image_color_label:
                raise ValueError("To calculate delta HSV across test and generated images,"
                                 " both images must concern the same color category!")

            test_image = test_image_label_pair[0]
            gen_image = generated_image_data[i][0]

            test_colors = [test_image[x, y, :] for x in range(test_image.shape[0] - 1)
                           for y in range(test_image.shape[1])]
            gen_colors = [gen_image[x, y, :] for x in range(gen_image.shape[0] - 1)
                          for y in range(gen_image.shape[1])]

            delta_hsv = [(self.calc_delta_hue(gen_colors[i][0], test_colors[i][0]),
                          abs(gen_colors[i][1] - test_colors[i][1]),
                          abs(gen_colors[i][2] - test_colors[i][2])) for i in range(len(gen_colors))]

            avg_hsv_distance = sum([math.sqrt(delta_hsv[i][0] * (delta_hsv[i][0] + delta_hsv[i][1])
                                              * (delta_hsv[i][1] + delta_hsv[i][2]) * delta_hsv[i][2])
                                    for i in range(len(delta_hsv))]) / len(gen_colors)

            batch_avg_delta_hsv.append(avg_hsv_distance)

            if delta_hsv_per_letter.get(test_image_label) is None:
                delta_hsv_per_letter.update({test_image_label: avg_hsv_distance})
            elif delta_hsv_per_color.get(test_image_color_label) is None:
                delta_hsv_per_color.update({test_image_color_label: avg_hsv_distance})
            else:
                delta_hsv_per_letter.update({test_image_label:
                                                 (delta_hsv_per_letter.get(test_image_label) + avg_hsv_distance) / 2})
                delta_hsv_per_color.update({test_image_color_label:
                                                (delta_hsv_per_letter.get(test_image_label) + avg_hsv_distance) / 2})

            i += 1

        return delta_hsv_per_letter, delta_hsv_per_color, sum(batch_avg_delta_hsv) / len(batch_avg_delta_hsv)

    @staticmethod
    def calc_delta_hue(hue1_gen, hue0_test):
        delta_hue = min(abs(hue1_gen * 360 - hue0_test * 360), 360 - abs(hue1_gen * 360 - hue0_test * 360)) / 180
        if delta_hue > 1:
            return 1
        elif delta_hue < 0:
            return 0
        else:
            return delta_hue

    # HARDCODED PLOTTING MEASURES. SCREW BOXPLOTS WITH A CAPITAL LETTER S.

    def plot_train_session_test_hsv_distance_and_train_loss(self, average_hsv_distance, losses,
                                                            hsv_distance_per_letter: dict, hsv_distance_per_color: dict,
                                                            loss_name):
        if isinstance(losses, list):
            plt.figure()
            plt.axes()
            plt.grid(which='both')
            plt.xlabel("Number of Epochs")
            plt.ylabel("{} / HSV Color Distance".format(loss_name))
            plt.plot(range(len(average_hsv_distance)), average_hsv_distance, label='test_hsv_distance')
            plt.plot(range(len(average_hsv_distance)), losses, label="train_{}".format(loss_name))
            plt.title("Average HSV Color Distance and {} change \n over test image batch per epoch".format(loss_name))
            plt.legend()
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_evaluation_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    len(average_hsv_distance)))
            plt.show()

            plt.figure()
            plt.axes()
            plt.grid(which='both')
            plt.xlabel("Number of Epochs")
            plt.ylabel("HSV Color Distance per letter".format(loss_name))
            epochs = 0
            for letter in hsv_distance_per_letter:
                values = hsv_distance_per_letter.get(letter)
                epochs = len(values)
                plt.plot(range(len(values)), values, label="{}-class".format(letter))
            plt.title("Average HSV Color Distance per letter over epochs".format(loss_name))
            plt.legend()
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perLetter_evaluation_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    epochs))
            plt.show()

            plt.figure()
            plt.axes()
            plt.grid(which='both')
            plt.xlabel("Number of Epochs")
            plt.ylabel("HSV Color Distance per Color".format(loss_name))
            epoch = 0
            for color in hsv_distance_per_color:
                values = hsv_distance_per_color.get(color)
                epoch = len(values)
                plt.plot(range(len(values)), values, label="{}-class".format(color))
            plt.title("Average HSV Color Distance per color over epochs".format(loss_name))
            plt.legend()
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perColor_evaluation_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()

            # ------------------------------- First Letter hsv distance Boxplot ----------------------------------------

            fig, ax = plt.subplots()
            plt.xlabel("Letter Class")
            plt.ylabel("Hsv-distance over {} epochs".format(epochs))
            values = np.asarray(list(hsv_distance_per_letter.values()))
            values = values[0:int(len(values)/2)]
            labels = np.asarray(list(hsv_distance_per_letter.keys()))
            labels = labels[0:int(len(labels)/2)]
            mins = np.asarray([min(value_list) for value_list in values])
            maxes = np.asarray([max(value_list) for value_list in values])
            means = np.asarray([np.asarray(value_list).mean() for value_list in values])
            std_dev = np.asarray([np.asarray(value_list).std() for value_list in values])

            bp = ax.boxplot(values.T, showmeans=True, positions=list(range(0, len(labels) * 2, 2)))
            plt.subplots_adjust(left=0.1, right=0.9, top=0.6, bottom=0.4)
            for i, line in enumerate(bp['medians']):
                x, y = line.get_xydata()[1]
                text = ' μ={:.2f}\n σ={:.2f} \n max={:.2f} \n min={:.2f}'.format(means[i - 1], std_dev[i - 1],
                                                                                 maxes[i - 1],
                                                                                 mins[i - 1])
                text = ax.annotate(text, xy=(x, y))
                text.set_fontsize(6)

            plt.xticks(list(range(0, len(labels)*2, 2)), labels)
            plt.xlim(-2, list(range(0, len(labels)*2+2, 2))[-1])
            plt.title("Box plots per letter class hsv-distance over {} Epochs".format(epochs))
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perLetter_boxPlots_{:04d}_epochs_part1.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()

            # ------------------------------- Second Letter hsv distance Boxplot ----------------------------------------
            fig, ax = plt.subplots()
            plt.xlabel("Letter Class")
            plt.ylabel("Hsv-distance over {} epochs".format(epochs))
            values = np.asarray(list(hsv_distance_per_letter.values()))
            values = values[int(len(values) / 2): len(values)]
            labels = np.asarray(list(hsv_distance_per_letter.keys()))
            labels = labels[int(len(labels) / 2): len(labels)]
            mins = np.asarray([np.amin(np.asarray(value_list)) for value_list in values])
            maxes = np.asarray([np.amax(np.asarray(value_list)) for value_list in values])
            means = np.asarray([np.asarray(value_list).mean() for value_list in values])
            std_dev = np.asarray([np.asarray(value_list).std() for value_list in values])

            bp = ax.boxplot(values.T, showmeans=True, positions=list(range(0, len(labels) * 2, 2)))
            plt.subplots_adjust(left=0.1, right=0.9, top=0.6, bottom=0.4)
            for i, line in enumerate(bp['medians']):
                x, y = line.get_xydata()[1]
                text = ' μ={:.2f}\n σ={:.2f} \n max={:.2f} \n min={:.2f}'.format(means[i - 1], std_dev[i - 1],
                                                                                 maxes[i - 1],
                                                                                 mins[i - 1])
                text = ax.annotate(text, xy=(x, y))
                text.set_fontsize(6)

            plt.xticks(list(range(0, len(labels)*2, 2)), labels)
            plt.xlim(-2, list(range(0, len(labels)*2+2, 2))[-1])
            plt.title("Box plots per letter class hsv-distance over {} Epochs".format(epochs))
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perLetter_boxPlots_{:04d}_epochs_part2.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()

            # ----------------------------------- Color hsv distance Boxplot -------------------------------------------

            fig, ax = plt.subplots()
            plt.xlabel("Color Class")
            plt.ylabel("Hsv-distance over {} epochs".format(epochs))
            values = np.asarray(list(hsv_distance_per_color.values()))
            labels = np.asarray(list(hsv_distance_per_color.keys()))
            mins = np.asarray([np.amin(np.asarray(value_list)) for value_list in values])
            maxes = np.asarray([np.amax(np.asarray(value_list)) for value_list in values])
            means = np.asarray([np.asarray(value_list).mean() for value_list in values])
            std_dev = np.asarray([np.asarray(value_list).std() for value_list in values])

            bp = ax.boxplot(values.T, showmeans=True, positions=list(range(0, len(labels) * 2, 2)))
            plt.subplots_adjust(left=0.1, right=0.9, top=0.6, bottom=0.4)

            for i, line in enumerate(bp['medians']):
                x, y = line.get_xydata()[1]
                text = ' μ={:.2f}\n σ={:.2f} \n max={:.2f} \n min={:.2f}'.format(means[i - 1], std_dev[i - 1],
                                                                                 maxes[i - 1],
                                                                                 mins[i - 1])
                ax.annotate(text, xy=(x, y))

            plt.xticks(list(range(0, len(labels)*2, 2)), labels)
            plt.xlim(-2, list(range(0, len(labels)*2+2, 2))[-1])
            plt.title("Box plots per color class hsv-distance over {} epochs".format(epochs))
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perColor_boxPlots_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()
        else:
            plt.figure()
            plt.axes()
            plt.grid(which='both')
            plt.xlabel("Number of Epochs")
            plt.ylabel("{} / HSV Color Distance".format(loss_name))
            plt.plot(range(len(average_hsv_distance)), average_hsv_distance, label='test_hsv_distance')
            plt.plot(range(len(average_hsv_distance)), losses[0], label="train_generator_{}".format(loss_name))
            plt.plot(range(len(average_hsv_distance)), losses[1], label="train_discriminator_{}".format(loss_name))
            plt.title("Average HSV Color Distance and {} change \n over test image batch per epoch".format(loss_name))
            plt.legend()
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_evaluation_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    len(average_hsv_distance)))
            plt.show()

            plt.figure()
            plt.axes()
            plt.grid(which='both')
            plt.xlabel("Number of Epochs")
            plt.ylabel("HSV Color Distance per letter")
            epochs = 0
            for letter in hsv_distance_per_letter:
                values = hsv_distance_per_letter.get(letter)
                epochs = len(values)
                plt.plot(range(len(values)), values, label="{}-class".format(letter))
            plt.title("Average HSV Color Distance per letter over epochs".format(loss_name))
            plt.legend()
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perLetter_evaluation_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    epochs))
            plt.show()

            plt.figure()
            plt.axes()
            plt.grid(which='both')
            plt.xlabel("Number of Epochs")
            plt.ylabel("HSV Color Distance per Color")
            epoch = 0
            for color in hsv_distance_per_color:
                values = hsv_distance_per_color.get(color)
                epoch = len(values)
                plt.plot(range(len(values)), values, label="{}-class".format(color))
            plt.title("Average HSV Color Distance per color over epochs".format(loss_name))
            plt.legend()
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perColor_evaluation_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()

            # ------------------------------- First Letter hsv distance Boxplot ----------------------------------------

            fig, ax = plt.subplots()
            plt.xlabel("Letter Class")
            plt.ylabel("Hsv-distance over {} epochs".format(epochs))
            values = np.asarray(list(hsv_distance_per_letter.values()))
            values = values[0:int(len(values) / 2)]
            labels = np.asarray(list(hsv_distance_per_letter.keys()))
            labels = labels[0:int(len(labels) / 2)]
            mins = np.asarray([np.amin(np.asarray(value_list)) for value_list in values])
            maxes = np.asarray([np.amax(np.asarray(value_list)) for value_list in values])
            means = np.asarray([np.asarray(value_list).mean() for value_list in values])
            std_dev = np.asarray([np.asarray(value_list).std() for value_list in values])

            bp = ax.boxplot(values.T, showmeans=True, positions=list(range(0, len(labels) * 2, 2)))
            plt.subplots_adjust(left=0.1, right=0.9, top=0.6, bottom=0.4)
            for i, line in enumerate(bp['medians']):
                x, y = line.get_xydata()[1]
                text = ' μ={:.2f}\n σ={:.2f} \n max={:.2f} \n min={:.2f}'.format(means[i - 1], std_dev[i - 1],
                                                                                 maxes[i - 1],
                                                                                 mins[i - 1])
                text = ax.annotate(text, xy=(x, y))
                text.set_fontsize(6)

            plt.xticks(list(range(0, len(labels) * 2, 2)), labels)
            plt.xlim(-2, list(range(0, len(labels) * 2 + 2, 2))[-1])
            plt.title("Box plots per letter class hsv-distance over {} Epochs".format(epochs))
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perLetter_boxPlots_{:04d}_epochs_part1.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()

            # ------------------------------- Second Letter hsv distance Boxplot ----------------------------------------
            fig, ax = plt.subplots()
            plt.xlabel("Letter Class")
            plt.ylabel("Hsv-distance over {} epochs".format(epochs))
            values = np.asarray(list(hsv_distance_per_letter.values()))
            values = values[int(len(values) / 2): len(values)]
            labels = np.asarray(list(hsv_distance_per_letter.keys()))
            labels = labels[int(len(labels) / 2): len(labels)]
            mins = np.asarray([np.amin(np.asarray(value_list)) for value_list in values])
            maxes = np.asarray([np.amax(np.asarray(value_list)) for value_list in values])
            means = np.asarray([np.asarray(value_list).mean() for value_list in values])
            std_dev = np.asarray([np.asarray(value_list).std() for value_list in values])

            bp = ax.boxplot(values.T, showmeans=True, positions=list(range(0, len(labels) * 2, 2)))
            plt.subplots_adjust(left=0.1, right=0.9, top=0.6, bottom=0.4)
            for i, line in enumerate(bp['medians']):
                x, y = line.get_xydata()[1]
                text = ' μ={:.2f}\n σ={:.2f} \n max={:.2f} \n min={:.2f}'.format(means[i - 1], std_dev[i - 1],
                                                                                 maxes[i - 1],
                                                                                 mins[i - 1])
                text = ax.annotate(text, xy=(x, y))
                text.set_fontsize(6)

            plt.xticks(list(range(0, len(labels) * 2, 2)), labels)
            plt.xlim(-2, list(range(0, len(labels) * 2 + 2, 2))[-1])
            plt.title("Box plots per letter class hsv-distance over {} Epochs".format(epochs))
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perLetter_boxPlots_{:04d}_epochs_part2.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()

            # ----------------------------------- Color hsv distance Boxplot -------------------------------------------

            fig, ax = plt.subplots()
            plt.xlabel("Color Class")
            plt.ylabel("Hsv-distance over {} epochs".format(epochs))
            values = np.asarray(list(hsv_distance_per_color.values()))
            labels = np.asarray(list(hsv_distance_per_color.keys()))
            mins = np.asarray([np.amin(np.asarray(value_list)) for value_list in values])
            maxes = np.asarray([np.amax(np.asarray(value_list)) for value_list in values])
            means = np.asarray([np.asarray(value_list).mean() for value_list in values])
            std_dev = np.asarray([np.asarray(value_list).std() for value_list in values])

            bp = ax.boxplot(values.T, showmeans=True, positions=list(range(0, len(labels) * 2, 2)))
            plt.subplots_adjust(left=0.1, right=0.9, top=0.6, bottom=0.4)

            for i, line in enumerate(bp['medians']):
                x, y = line.get_xydata()[1]
                text = ' μ={:.2f}\n σ={:.2f} \n max={:.2f} \n min={:.2f}'.format(means[i - 1], std_dev[i - 1],
                                                                                 maxes[i - 1],
                                                                                 mins[i - 1])
                ax.annotate(text, xy=(x, y))

            plt.xticks(list(range(0, len(labels) * 2, 2)), labels)
            plt.xlim(-2, list(range(0, len(labels) * 2 + 2, 2))[-1])
            plt.title("Box plots per color class hsv-distance over {} epochs".format(epochs))
            plt.savefig(
                'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_perColor_boxPlots_{:04d}_epochs.png'.format(
                    self.__class__.__name__,
                    epoch))
            plt.show()
