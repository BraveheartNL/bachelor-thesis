from __future__ import print_function, division

import itertools
import string

from skimage import color
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pickle
import types
from colormath.color_objects import LabColor
from colormath.color_diff import delta_e_cie2000

# matplotlib inline
plt.rcParams['figure.figsize'] = (10.0, 8.0)  # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'


# A bunch of utility functions

def show_images(images):
    images = np.reshape(images, [images.shape[0], -1])  # images reshape to (batch_size, D)
    sqrtn = int(np.ceil(np.sqrt(images.shape[0])))
    sqrtimg = int(np.ceil(np.sqrt(images.shape[1])))

    fig = plt.figure(figsize=(sqrtn, sqrtn))
    gs = gridspec.GridSpec(sqrtn, sqrtn)
    gs.update(wspace=0.05, hspace=0.05)

    for i, img in enumerate(images):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        plt.imshow(img.reshape([sqrtimg, sqrtimg]))
    return


def preprocess_img(x):
    return 2 * x - 1.0


def deprocess_img(x):
    return (x + 1.0) / 2.0


def get_session():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    return session


def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo)
    return dict


def extract_single_dim_from_LAB_convert_to_RGB(image, idim):
    '''
    image is a single lab image of shape (None,None,3)
    '''
    z = np.zeros(image.shape)
    if idim != 0:
        z[:, :, 0] = 80  ## I need brightness to plot the image along 1st or 2nd axis
    z[:, :, idim] = image[:, :, idim]
    z = color.lab2rgb(z)
    return (z)


def plot_lab_batch_layers(ver_train_dataset):
    nsample = ver_train_dataset.shape[0]
    fig = plt.figure(figsize=(13, 3 * nsample))
    count = 1
    for lab in ver_train_dataset:
        ax = fig.add_subplot(nsample, 3, count)
        lab_rgb_gray = extract_single_dim_from_LAB_convert_to_RGB(lab, 0)
        ax.imshow(lab_rgb_gray)
        ax.axis("off")
        ax.set_title("L: lightness")
        count += 1

        ax = fig.add_subplot(nsample, 3, count)
        lab_rgb_gray = extract_single_dim_from_LAB_convert_to_RGB(lab, 1)
        ax.imshow(lab_rgb_gray)
        ax.axis("off")
        ax.set_title("A: color spectra green to red")
        count += 1

        ax = fig.add_subplot(nsample, 3, count)
        lab_rgb_gray = extract_single_dim_from_LAB_convert_to_RGB(lab, 2)
        ax.imshow(lab_rgb_gray)
        ax.axis("off")
        ax.set_title("B: color spectra blue to yellow")
        count += 1

    plt.show()


def plot_ver_rgb_batch(ver_train_dataset):
    plt.figure(figsize=(10, 10))
    for n in range(25):
        ax = plt.subplot(5, 5, n + 1)
        plt.imshow(ver_train_dataset[n])
        plt.axis('off')
    plt.show()


def plot_ver_grey_batch(ver_grey_dataset, data_shape):
    plt.figure(figsize=(10, 10))
    for n in range(25):
        ax = plt.subplot(5, 5, n + 1)
        plt.imshow(ver_grey_dataset[n].reshape(data_shape), cmap='gray')
        plt.axis('off')
    plt.show()


def plot_ver_hsv_batch(ver_train_dataset):
    plt.figure(figsize=(10, 10))
    for n in range(25):
        ax = plt.subplot(5, 5, n + 1)
        plt.imshow(color.hsv2rgb(ver_train_dataset[n]))
        plt.axis('off')
    plt.show()


def extract_original_images_in_dimension(data_set, data_shape):
    data_shape_grey = list(data_shape)
    data_shape_grey.append(1)
    data_shape_grey = tuple(data_shape_grey)
    data_shape_color = list(data_shape)
    data_shape_color.append(3)
    data_shape_color = tuple(data_shape_color)

    # print(data_set.ndim)

    if data_set.shape[1] == 2:
        grey_images = np.asarray(
            [np.reshape(image_couple[0], data_shape_grey) for image_couple in data_set])
        color_images = np.asarray(
            [np.reshape(image_couple[1], data_shape_color) for image_couple in data_set])
        return grey_images, color_images
    elif data_set.shape[1] == 3:
        grey_images = np.asarray(
            [[np.reshape(image_data[0], data_shape_grey), image_data[2][1]] for image_data in data_set])
        color_images = np.asarray(
            [[np.reshape(image_data[1], data_shape_color), image_data[2]] for image_data in data_set])
        return grey_images, color_images
    else:
        return data_set


def get_single_random_batch(batch_generator: types.GeneratorType):
    return next(itertools.islice(batch_generator, 1))


def calculate_avg_across_batch_delta_e(image_batch_1: np.ndarray, image_batch_2: np.ndarray, input_shape: tuple):
    _, color_batch_images_1 = extract_original_images_in_dimension(
        image_batch_1, input_shape)

    _, color_batch_images_2 = extract_original_images_in_dimension(
        image_batch_2, input_shape)

    if color_batch_images_1.shape != color_batch_images_2.shape:
        raise ValueError("both batches do not have equal shape!")
    elif color_batch_images_1.shape[1] == 2:
        color_batch_images_1 = color_batch_images_1[:, 0]
        color_batch_images_2 = color_batch_images_2[:, 0]

    batch_avg_delta_e = []

    i = 0
    for colimage_from_batch1 in color_batch_images_1:
        batch1_colors = [LabColor(lab_l=c[0], lab_a=c[1], lab_b=c[2]) for c in
                         [colimage_from_batch1[x, y, :] for x in range(
                             colimage_from_batch1.shape[0] - 1) for y in range(colimage_from_batch1.shape[1] - 1)]]
        batch2_colors = [LabColor(lab_l=c[0], lab_a=c[1], lab_b=c[2]) for c in
                         [color_batch_images_2[i][x, y, :] for x in range(
                             color_batch_images_2[i].shape[0] - 1) for y in
                          range(color_batch_images_2[i].shape[1] - 1)]]

        avg_delta_e = sum(
            [delta_e_cie2000(batch1_colors[x], batch2_colors[x]) for x in range(len(batch2_colors))]) / len(
            batch2_colors)
        batch_avg_delta_e.append(avg_delta_e)
        i += 1

    return sum(batch_avg_delta_e) / len(batch_avg_delta_e)


def extract_images_and_labels_from_batch(batch: np.ndarray):
    if batch.shape[1] != 2:
        raise ValueError("Unexpected dimensions {}, should be (batchsize, 2 - (images, labels))".format(batch.shape))
    else:
        batch_labels = batch[:, 1]
        batch_images = batch[:, 0]

    return batch_images, batch_labels


def merge_image_batch_with_label_batch(image_batch: np.ndarray, label_batch: np.ndarray):
    if image_batch.shape[0] != label_batch.shape[0]:
        raise ValueError("Merging first batch with second batch impossible, as they are not of the same size!")
    return np.asarray([[image_batch[i], label_batch[i]] for i in range(image_batch.shape[0])])


def plot_data_set_distribution(data_set, test: bool):
    amount_of_letters = dict.fromkeys(string.ascii_uppercase, 0)
    amount_of_colors = dict.fromkeys(['red', 'blue', 'green', 'yellow', 'orange', 'purple', 'white', 'black'], 0)
    for data in data_set:
        label = data[2][1]
        amount_of_letters.update({label: amount_of_letters.get(label) + 1})
        color = data[2][2]
        amount_of_colors.update({color: amount_of_colors.get(color) + 1})

    letter_values = np.asarray(list(amount_of_letters.values()))
    bins_letters = list(range(len(list(amount_of_letters.keys()))))

    color_values = np.asarray(list(amount_of_colors.values()))
    color_bins = list(range(len(list(amount_of_colors.keys()))))

    fig, ax = plt.subplots()
    plt.grid(which='both')
    ax.set_title("Amount of letter class occurences in {}-dataset".format("test" if test else "train"))
    ax.set_ylabel("Amount of occurences in {}-dataset".format("test" if test else "train"))
    bar_letters = ax.bar(bins_letters, letter_values)
    ax.set_xticks(list(range(len(list(amount_of_letters.keys())))))
    ax.set_xticklabels(list(amount_of_letters.keys()))

    autolabel(bar_letters, ax, 9)

    plt.savefig(
        'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_dataset_letter_evaluation.png'
            .format("test" if test else "train"))

    plt.show()

    fig, ax = plt.subplots()
    plt.grid(which='both')
    ax.set_title("Amount of color class occurences in {}-dataset".format("test" if test else "train"))
    ax.set_ylabel("Amount of occurences in {}-dataset".format("test" if test else "train"))
    bar_colors = ax.bar(color_bins, color_values)
    plt.xticks(list(range(len(list(amount_of_colors.keys())))), list(amount_of_colors.keys()))

    autolabel(bar_colors, ax, 20)

    plt.savefig(
        'D:\\swdev\\GItKrakenRepos\\bachelor-thesis\\Synesthesia_GAN\\Final_data\\{}_dataset_color_evaluation.png'
            .format("test" if test else "train"))
    plt.show()


def autolabel(rects, ax, text_size: int):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom').set_fontsize(text_size)
