from enum import Enum
from random import random, uniform
import string


class SynestheticLetterColorMap(Enum):
    red = (['A', 'M', 'R', 'H'], dict(h=uniform(0.958, 1), s=uniform(0.2, 1), v=uniform(0.3, 1))) #there's zero reds in the dataset..?
    blue = (['B', 'D', 'T', 'W'], dict(h=uniform(0.667, 0.75), s=uniform(0.2, 1), v=uniform(0.3, 1)))
    green = (['E', 'F', 'G'], dict(h=uniform(0.25, 38), s=uniform(0.2, 1), v=uniform(0.3, 1)))
    yellow = (['C', 'L', 'S', 'Y'], dict(h=uniform(0.167, 0.181), s=uniform(0.2, 1), v=uniform(0.3, 1)))
    orange = (['H', 'J', 'K', 'N', 'U'], dict(h=uniform(0.056, 0.0834), s=uniform(0.2, 1), v=uniform(0.3, 1)))
    purple = (['P', 'Q', 'V'], dict(h=uniform(0.778, 0.834), s=uniform(0.2, 1), v=uniform(0.3, 1)))
    white = (['I', 'O'], dict(h=0, s=0, v=uniform(0.85, 1)))
    black = (['X', 'Z'], dict(h=0, s=0, v=uniform(0.15, 1)))


class Synesthetic_color_letter_mapper(object):

    def __init__(self):
        self.color_letter_map = self.__initialize_letter_color_map()

    def get_char_synesthetic_color_hsv_values(self, idx: int):
        if isinstance(self.color_letter_map[idx], dict):
            return self.color_letter_map[idx].get("hsv_values")
        else:
            raise NotImplementedError("{} - Only Capital letters support Synesthetic Color conversion at this time! "
                                      "Select a suitable dataset for synesthetic conversion!".format(
                self.__class__.__name__))

    def get_char_synesthetic_color_name(self, idx: int):
        if isinstance(self.color_letter_map[idx], dict):
            return self.color_letter_map[idx].get("color")
        else:
            raise NotImplementedError("{} - Only Capital letters support Synesthetic Color conversion at this time! "
                                      "Select a suitable dataset for synesthetic conversion!".format(
                self.__class__.__name__))

    def __initialize_letter_color_map(self):
        color_letter_map = list(string.digits)  # insert place holders for non relevant characters (for now)
        for char in list(string.ascii_uppercase):
            for (color, value) in [(e.name, e.value) for e in SynestheticLetterColorMap]:
                if char in value[0]:
                    color_letter_map.append(dict(char=char, color=color, hsv_values=value[1]))
                    break
        color_letter_map = color_letter_map + list(
            string.ascii_lowercase)  # insert place holders for non relevant characters (for now)
        return color_letter_map
