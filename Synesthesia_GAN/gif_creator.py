import glob
from datetime import date

import imageio
from IPython import display


class gif_creator(object):

    def __init__(self, file_name_pattern, file_path):
        self.file_name_pattern = file_name_pattern + "\\"
        self.anim_file = file_path + '\\progress2_{}.gif'.format(date.today())

    def create_and_store_gif(self):
        with imageio.get_writer(self.anim_file, mode='I') as writer:
            filenames = glob.glob('{}*.png'.format(self.file_name_pattern))
            filenames = sorted(filenames)
            last = -1
            for i, filename in enumerate(filenames):
                frame = 2 * (i ** 0.5)
                if round(frame) > round(last):
                    last = frame
                else:
                    continue
                image = imageio.imread(filename)
                writer.append_data(image)
            image = imageio.imread(filename)
            writer.append_data(image)

        import IPython
        if IPython.version_info > (6, 2, 0, ''):
            display.Image(filename=self.anim_file)
