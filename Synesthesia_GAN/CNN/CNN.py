import time
import tensorflow as tf
import matplotlib.pyplot as plt
from Synesthesia_GAN.CNN.CNN_network import CNN_network
import PIL.Image
from skimage import color
import numpy as np
from IPython import display
import random
from colormath.color_objects import LabColor
from colormath.color_diff import delta_e_cie2000
from Synesthesia_GAN.interfaces.ISynestheticNeuralNet import ISynestheticNeuralNet
import Synesthesia_GAN.Utilities as utils


class CNN(ISynestheticNeuralNet, object):

    def __init__(self, input_shape: [int]):
        print("---------------------- INITIALIZING CNN ---------------------")
        self.network = CNN_network(input_shape)
        self.network_optimizer = tf.keras.optimizers.Adam(1e-3)
        self.checkpoint_dir = './Training_checkpoints/CNN_training_checkpoints'
        self.input_shape = input_shape

        print("------------------- INITIALIZING CNN DONE -------------------")

    def train(self, train_dataset, epochs, test_dataset, no_of_batches_per_epoch):
        train_session_test_delta_hsv_per_letter = {}
        train_session_test_delta_hsv_per_color = {}
        train_session_test_delta_hsv = []
        train_session_MSE = []

        # if no_of_batches_per_epoch * epochs > total_batches:
        #     raise ValueError("Number of batches required for all epochs is higher than total batches provided by generator!")
        #
        # elif no_of_batches_per_epoch <= 0 or total_batches <= 0:
        #     raise ValueError("Each epoch must yield minimally 1 batch. Increase \'no_of_batches_per_epoch\' and/or \'total_batches\'!")

        print("{} - Start training network and evaluating within epochs....".format(
            self.__class__.__name__))

        for epoch in range(epochs):
            start = time.time()
            epoch_mse = []

            i = no_of_batches_per_epoch
            while i != 0:

                image_batch = next(train_dataset)
                grey_train_batch, color_train_batch = utils.extract_original_images_in_dimension(image_batch,
                                                                                                 self.input_shape)

                grey_train_images, grey_train_labels = utils.extract_images_and_labels_from_batch(grey_train_batch)
                color_train_images, color_train_labels = utils.extract_images_and_labels_from_batch(color_train_batch)

                grey_train_images = tf.convert_to_tensor([image * (1.0 / 255.0) for image in grey_train_images], dtype=tf.float64)
                color_train_images = tf.convert_to_tensor(color_train_images.tolist())

                avg_image_batch_mse = np.average(self.train_step(grey_train_images, color_train_images).numpy())
                epoch_mse.append(avg_image_batch_mse)

                i -= 1

            display.clear_output(wait=True)

            delta_hsvs = self.generate_and_save_images(self.network, epoch + 1,
                                                       utils.get_single_random_batch(test_dataset))

            train_session_MSE.append(np.average(epoch_mse))
            train_session_test_delta_hsv.append(delta_hsvs[2])

            for key in delta_hsvs[0]:
                if isinstance(train_session_test_delta_hsv_per_letter.get(key), list) \
                        and bool(train_session_test_delta_hsv_per_letter.get(key)) and delta_hsvs[0].get(key):
                    train_session_test_delta_hsv_per_letter.update(
                        {key: train_session_test_delta_hsv_per_letter.get(key)
                              + [delta_hsvs[0].get(key)]})
                else:
                    if delta_hsvs[0].get(key):
                        train_session_test_delta_hsv_per_letter.update({key: [delta_hsvs[0].get(key)]})

            for key in delta_hsvs[1]:
                if isinstance(train_session_test_delta_hsv_per_color.get(key), list) \
                        and bool(train_session_test_delta_hsv_per_color.get(key)) and delta_hsvs[1].get(key):
                    train_session_test_delta_hsv_per_color.update(
                        {key: train_session_test_delta_hsv_per_color.get(key)
                              + [delta_hsvs[1].get(key)]})
                else:
                    if delta_hsvs[1].get(key):
                        train_session_test_delta_hsv_per_color.update({key: [delta_hsvs[1].get(key)]})

            print('{} - Training time for epoch {} is {:.2f} sec'.format(self.__class__.__name__, epoch + 1,
                                                                         time.time() - start))

        # Generate after the final epoch
        display.clear_output(wait=True)
        self.generate_and_save_images(self.network, epochs, utils.get_single_random_batch(test_dataset))

        print("{} - Plotting evaluation measures....".format(self.__class__.__name__))
        self.plot_train_session_test_hsv_distance_and_train_loss(train_session_test_delta_hsv, train_session_MSE,
                                                                 train_session_test_delta_hsv_per_letter,
                                                                 train_session_test_delta_hsv_per_color,
                                                                 "L2")

        print("{} - Done training network and evaluating within epochs!".format(
            self.__class__.__name__))

    @tf.function
    def train_step(self, grey_images, color_images):

        with tf.GradientTape() as cnn_tape:
            generated_output = self.network(grey_images, training=True)
            network_loss = self.network.loss(generated_output, color_images)

            gradients_of_network = cnn_tape.gradient(network_loss, self.network.trainable_variables)

        self.network_optimizer.apply_gradients(zip(gradients_of_network, self.network.trainable_variables))

        return network_loss
