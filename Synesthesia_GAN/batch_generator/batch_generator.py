import numpy as np


class BatchGenerator(object):

    def __init__(self, batch_size):
        self.batch_size = batch_size

    def get_batched_dataset(self, image_set):
        indices = np.arange(len(image_set))
        batch = []
        while True:
            np.random.shuffle(indices)
            for i in indices:
                batch.append(i)
                if len(batch) == self.batch_size:
                    x = image_set[batch]
                    yield x
                    batch = []
